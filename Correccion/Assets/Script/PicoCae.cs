﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicoCae : MonoBehaviour
{

    public Rigidbody2D rigidbody;

    private void Awake() {
        rigidbody.bodyType = RigidbodyType2D.Kinematic;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {
            rigidbody.bodyType = RigidbodyType2D.Dynamic;
        
        
        }
    
    
    
    }


}
