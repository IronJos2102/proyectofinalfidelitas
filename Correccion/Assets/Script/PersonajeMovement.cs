﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PersonajeMovement : MonoBehaviour
{
    public Rigidbody2D rigidbody2D;
    [SerializeField]
    private float _Velocidad;
    [SerializeField]
    private Collider2D _floorSensor;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rigidbody2D.velocity = new Vector2(_Velocidad, rigidbody2D.velocity.y);

        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rigidbody2D.velocity = new Vector2(-_Velocidad, rigidbody2D.velocity.y);
        }
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {

            EvaluateFloor(_floorSensor);
        }

    }

    private void EvaluateFloor(Collider2D pCollider)
    {
        RaycastHit2D[] hits = Physics2D.BoxCastAll(pCollider.bounds.center, pCollider.bounds.size, 0f, Vector2.zero);

        foreach (RaycastHit2D singleHit in hits)
        {
            if (singleHit.collider.tag == "Floor")
            {
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 5f);

            }
            
        }
    }


        public void death()
    {
        //Destroy(this.gameObject);
        SceneManager.LoadScene("Lvl 1");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Velocidad") {
            Destroy(collision.gameObject);
            _Velocidad = 6f;
        
        
        }
    }
}
