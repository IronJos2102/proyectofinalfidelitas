﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonScript : MonoBehaviour
{
    public GameObject BalaCannon;
    
    // Start is called before the first frame update
    void Start()
    {
       

    }
    public float ratioDisparo = 5f;
    private float _tiempoHastaElMomento = 0f;

    // Update is called once per frame
    void Update()
    {
        _tiempoHastaElMomento += Time.deltaTime;

        if (_tiempoHastaElMomento > ratioDisparo)
        {
            _tiempoHastaElMomento -= ratioDisparo;

         
            GameObject bala = GameObject.Instantiate(BalaCannon);

            bala.transform.position = this.transform.position+new Vector3(-1f, 0, 0);

            Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
            balaRigidbody.velocity = new Vector3(-3f,0,0);
        }
    }
}
