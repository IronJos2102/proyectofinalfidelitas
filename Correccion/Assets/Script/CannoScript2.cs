﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannoScript2 : MonoBehaviour
{
    public GameObject BalaCannon;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public float ratioDisparo = 5f;
    private float _tiempoHastaElMomento = 0f;


    // Update is called once per frame
    void Update()
    {
        _tiempoHastaElMomento += Time.deltaTime;

        if (_tiempoHastaElMomento > ratioDisparo)
        {
            _tiempoHastaElMomento -= ratioDisparo;

            //Se usa Instantiate con parametro el gameobject que quiero crear
            GameObject bala = GameObject.Instantiate(BalaCannon);

            //esto para ponerle la posicion a mano
            bala.transform.position = this.transform.position + new Vector3(1f, 0, 0);

            Rigidbody2D balaRigidbody = bala.GetComponent<Rigidbody2D>();
            balaRigidbody.velocity = new Vector3(3f, 0, 0);
        }
    }
}
