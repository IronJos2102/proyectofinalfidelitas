﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeMuerte : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {


            Destroy(this.gameObject);
            if (collision.gameObject.GetComponent<PersonajeMovement>() != null)
            {
                collision.gameObject.GetComponent<PersonajeMovement>().death();
            }


        }

        if (collision.tag == "Floor") {
            Destroy(this.gameObject);
        
        }
       
    }
}
