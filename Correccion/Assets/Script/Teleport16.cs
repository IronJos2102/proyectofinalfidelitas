﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport16 : MonoBehaviour
{
  
    public GameObject teleport16;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        collision.gameObject.transform.position = teleport16.transform.position;


    }
}

