﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampilla : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {


        if (Input.GetKey(KeyCode.UpArrow))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity += new Vector2(10f, 20f);
        }
        else
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity += new Vector2(5f, 10f);
        }
    }
}
